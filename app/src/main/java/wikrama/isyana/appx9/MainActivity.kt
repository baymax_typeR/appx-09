package wikrama.isyana.appx9

import android.database.Cursor
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    lateinit var db : SQLiteDatabase

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay ->{
                audioPlay(idMusik,idCover,idJudul)
            }
            R.id.btnNext ->{
                audioNext()
            }
            R.id.btnPrev ->{
                audioPrev()
            }
            R.id.btnStop ->{
                audioStop()
            }
        }
    }

    val daftarLagu = intArrayOf(R.raw.anysong, R.raw.lethergo, R.raw.memories)
    val daftarJudul = arrayOf("Any Song", "Let Her Go", "Memories")
    val daftarCover = intArrayOf(R.drawable.img1, R.drawable.img2, R.drawable.img3)
    val daftarVideo = intArrayOf(R.raw.video1, R.raw.video2, R.raw.video3)

    lateinit var adapter : ListAdapter
    lateinit var  v : View

    var posLaguSkrg = 0
    var posVideoSkrg = 0
    var posCoverSkr = 0
    var posJudulSkr = ""
    var handler = Handler()
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController
    var idMusik : Int = 0x7f0e0002
    var idCover : Int = 0x7f070063
    var idJudul : String = "Maroon 5 - Memories"
    var playM : Int = idMusik
    var playC : Int = idCover
    var playJ : String = idJudul
    var max : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        db = DBOpenHelper(this).writableDatabase
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        seekSong.max = 100
        seekSong.progress = 0
        seekSong.setOnSeekBarChangeListener(this)
        btnPlay.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        mediaController.setPrevNextListeners(nextVid,prevVid)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        videoSet(posVideoSkrg)
        lsMp.setOnItemClickListener(itemClicked)
    }
    var nextVid = View.OnClickListener { v: View? ->
        if (posVideoSkrg<(daftarVideo.size-1)) posVideoSkrg++
        else posVideoSkrg = 0
        videoSet(posVideoSkrg)
    }
    var prevVid = View.OnClickListener { v: View? ->
        if (posVideoSkrg>0) posVideoSkrg--
        else posVideoSkrg = daftarVideo.size-1
        videoSet(posVideoSkrg)
    }
    fun videoSet(pos: Int){
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarVideo[pos]))
    }
    
    fun milliSecondToString(ms : Int):String {
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        var menit = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioPlay(a:Int,b:Int,c:String){
        mediaPlayer.stop()
        mediaPlayer = MediaPlayer.create(this,a)
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imgView.setImageResource(b)
        txJudulLagu.setText(idJudul)
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread, 50)
    }
    fun play(){
        posLaguSkrg = playM
        posCoverSkr = playC
        posJudulSkr = playJ
    }

    fun audioNext(){
        if(mediaPlayer.isPlaying) {
            mediaPlayer.stop()
        }
        if(posLaguSkrg < 0x7f0e0002){
            posLaguSkrg++
            posCoverSkr++
            jud(posLaguSkrg)
        } else {
            posLaguSkrg = 0x7f0e0000
            posCoverSkr = 0x7f070061
            jud(posLaguSkrg)
        }
        audioPlay(posLaguSkrg,posCoverSkr,posJudulSkr)
    }
    fun jud(a:Int){
        if(a==0x7f0c0000){
            idJudul="Zico - Any Song"

        }
        else if(a==0x7f0c0001){
            idJudul="Passenger - Let Her Go"
        }
        else{
            idJudul="Maroon 5 - Memories"
        }
    }

    fun audioPrev(){
        if(mediaPlayer.isPlaying) {
            mediaPlayer.stop()
        }
        if(posLaguSkrg > 0x7f0e0000){
            posLaguSkrg--
            posCoverSkr--
            jud(posLaguSkrg)
        } else {
            posLaguSkrg = 0x7f0e0002
            posCoverSkr = 0x7f070063
            jud(posLaguSkrg)
        }
        audioPlay(posLaguSkrg,posCoverSkr,posJudulSkr)
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying) {
            mediaPlayer.stop()
        }
    }

    fun showDataMp(){
        var sql = "select id_music as _id, id_cover as cover, music_title as judul from mp"
        max = "select count(id_music) as i from mp"
        val c : Cursor = db.rawQuery(sql,null)
        adapter = SimpleCursorAdapter(this,R.layout.isi_musik,c, arrayOf("_id","cover","judul"),
            intArrayOf(R.id.txtIdMusik,R.id.txtIdCover,R.id.txtMusikTitle),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsMp.adapter = adapter
    }
    val itemClicked = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        idMusik = c.getInt(c.getColumnIndex("_id"))
        txJudulLagu.setText(c.getString(c.getColumnIndex("judul")))
        idCover = c.getInt(c.getColumnIndex("cover"))
        idJudul = c.getString(c.getColumnIndex("judul"))
        audioPlay(idMusik,idCover,idJudul)
    }

    fun getDBObject():SQLiteDatabase{
        return db
    }

    override fun onStart() {
        super.onStart()
        showDataMp()
        play()
    }

    inner class UpdateSeekBarProgressThread : Runnable {
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress = currTime
            if(currTime != mediaPlayer.duration){
                handler.postDelayed(this, 50)
            }
        }
    }
}
