package wikrama.isyana.appx9

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context) : SQLiteOpenHelper (context, DB_Name,null, DB_Ver){
    override fun onCreate(db: SQLiteDatabase?) {
        val tMp = "create table mp(id_music text, id_cover text, music_title text)"
        val insMp = "insert into mp values('0x7f0e0000', '0x7f070061', 'Zico - Any Song'), ('0x7f0e0001', '0x7f070062', 'Passenger - Let Her Go'), ('0x7f0e0002', '0x7f070063', 'Maroon 5 - Memories')"
        db?.execSQL(tMp)
        db?.execSQL(insMp)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "app9"
        val DB_Ver = 1
    }

}